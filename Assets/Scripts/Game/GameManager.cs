﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {
    public static GameManager instance;

    public bool isMainMenu;
    public UIMenuController.MainMenus startUI;

    public void Awake () {
        instance = this;
    }

    private void Start () {
        UIFadeTransitionController.instance.FadeUIOutDelay(0.75f, 1.5f);
        if (isMainMenu) {
            LobbyPlayerManager.instance.SetUp();
            UNETChat.instance.Clear();
        }

        if ((MyLobbyPlayer.localPlayer == null || (MyLobbyPlayer.localPlayer != null && !MyLobbyPlayer.localPlayer.GetReady())) && startUI == UIMenuController.MainMenus.NoMenu) UIMenuController.instance.SetMenu(UIMenuController.MainMenus.LobbyMenu);
        else UIMenuController.instance.SetMenu(startUI);
    }

    public void Quit () {
        Application.Quit();
    }

}