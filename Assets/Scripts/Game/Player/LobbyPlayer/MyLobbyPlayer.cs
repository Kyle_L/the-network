﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class MyLobbyPlayer : NetworkBehaviour {
    public static MyLobbyPlayer localPlayer;

    [SyncVar]
    private bool isInGame;

    [SyncVar(hook = "OnMyName")]
    public string playerName = "";

    [SyncVar(hook = "OnMyCharacter")]
    public int myCharacterEnumIndex;

    public InputField playerNameText;
    public Image playerCharacterImage;
    public Image background;
    public Color localPlayerColor;

    public Sprite[] images;

    private void Awake() {
        DontDestroyOnLoad(this);

        if (isLocalPlayer) localPlayer = this;
    }

    private void Start() {
        SetUp();
    }

    public void SetUp() {

        SetParent();
        LobbyPlayerManager.instance.AddPlayer(this);

        if (isLocalPlayer) {
            SetUpLocalPlayer();
        } else {
            SetUpOtherPlayer();
        }

        //setup the player data on UI. The value are SyncVar so the player
        //will be created with the right value currently on server
        OnMyName(playerName);
        OnMyCharacter(myCharacterEnumIndex);
    }

    public void SetParent() {
        //if (!isServer) return;
        //CmdSetParent();
        gameObject.transform.SetParent(UI_Lobby.instance.playerListContentTransform, false);
    }

    [Command]
    private void CmdSetParent () {
        RpcSetParent();
    }

    [ClientRpc]
    private void RpcSetParent () {
        gameObject.transform.SetParent(UI_Lobby.instance.playerListContentTransform, false);
    }

    #region SetName

    public void SetName (string newName) {
        if (!hasAuthority) return;

        /* This is only on the local client. */
        newName = StringChecker.RemoveRichText(newName);
        playerNameText.text = newName;
        playerName = newName;

        /* Updates the name on the server. */
        CmdSetName(newName);
    }

    [Command]
    private void CmdSetName (string newName) {
        if (!isServer) return;
        playerName = newName;
    }

    private void OnMyName(string newName) {
        if (isLocalPlayer) return;
        /*Changes the text everywhere else but on the local client.
        because it should already be correct on the local client since it
        was changed in SetName*/
        playerName = newName;
        playerNameText.text = newName;
    }

    #endregion

    #region SetCharacter

    public void SetCharacter (Player.Character newCharacter) {
        if (!hasAuthority) return;
        CmdSetCharacter(newCharacter);
    }

    [Command]
    private void CmdSetCharacter(Player.Character newCharacter) {
        if (!isServer) return;
        myCharacterEnumIndex = (int) newCharacter;
    }

    private void OnMyCharacter(int newCharacter) {
        myCharacterEnumIndex = newCharacter;
        ChangeCharacterIcon((Player.Character) newCharacter);
    }

    public void ChangeCharacterIcon(Player.Character aCharacter) {
        switch (aCharacter) {
            case Player.Character.Null:
                playerCharacterImage.sprite = null;
                break;
            case Player.Character.One:
                playerCharacterImage.sprite = images[0];
                break;
            case Player.Character.Two:
                playerCharacterImage.sprite = images[1];
                break;
            case Player.Character.Three:
                playerCharacterImage.sprite = images[2];
                break;
            case Player.Character.Four:
                playerCharacterImage.sprite = images[3];
                break;
            default:
                break;
        }
    }

    #endregion

    public void SetUpLocalPlayer () {
        localPlayer = this;
        playerNameText.enabled = true;
        background.color = localPlayerColor;
    }

    public void SetUpOtherPlayer() {
        playerNameText.enabled = false;
    }

    [Command]
    public void CmdEnterGame() {
        MyNetworkManager.singleton.EnterGame(this);
        LobbyPlayerManager.instance.SetPlayerReady(this);
    }

    public void SetInGame () {
        isInGame = true;
    }

    public bool GetReady () {
        return isInGame;
    }
}
