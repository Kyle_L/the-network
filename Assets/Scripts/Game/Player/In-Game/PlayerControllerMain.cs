﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerControllerMain : NetworkBehaviour {
    public static PlayerControllerMain instance;

    //Controls
    private bool canControl = true;

    public PlayerMovementController playerMovementController;
    public PlayerCustomizationController playerCustomizationController;
    public PlayerNetworkController playerNetworkController;
    public PlayerSettingsController playerSettingsController;

    public void Start() {

        if (!isLocalPlayer) return;
        instance = this;

        playerMovementController = GetComponent<PlayerMovementController>();
        playerCustomizationController = GetComponent<PlayerCustomizationController>();
        playerNetworkController = GetComponent<PlayerNetworkController>();
        playerSettingsController = GetComponent<PlayerSettingsController>();
    }

    void Update() {

        if (!isLocalPlayer) return;
        //if gameplay close, then if menu close. check game before menu.
        if (Input.GetButtonDown("Cancel")) {

        }



    }

    public void SetControl(bool control) {
        canControl = control;
        playerMovementController.SetControl(control);
    }

    public bool GetControl() {
        return canControl;
    }

}
