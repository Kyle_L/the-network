﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[RequireComponent(typeof(CharacterController))]
public class PlayerMovementController : NetworkBehaviour {

    //Components
    private CharacterController _characterController;
    private Animator _animator;

    //Camera rotation
    [Header("Camera Rotation Settings")]
    public float upDownRange = 80f;
    private float xRotation;
    private float yRotation;
    private float currentXRotation;
    private float currentYRotation;
    private float xRotationV;
    private float yRotationV;
    private float rotationSmoothTime = 0.075f;
    public float currentLookSensitivity = 10;

    // Movement Controller Variables
    [Header("Movement Speed Settings")]
    public float moveSpeed = 5;
    public float sideSpeedMultiplier = 0.75f;

    //Jumping
    [Header("Jumping Settings")]
    private float verticalVelocity = 0f;
    public float jumpSpeed = 2.5f;
    public float jumpHeightDetection = 0.5f;

    public Camera playerMainCamera; 

    private bool canControl = true;

    private void Start () {
        if (!isLocalPlayer) return;

        //Gets componenets.
        _characterController = GetComponent<CharacterController>();
        _animator = GetComponent<Animator>();

        //Default look direction.
        SetRotation(transform.localEulerAngles.y);
    }

    private void Update () {
        if (!isLocalPlayer) return;

        #region Movement
        float forwardSpeed = 0;
        float sideSpeed = 0;

        if (canControl) {
            //Gets character movement input

            //Rotation
            xRotation -= Input.GetAxis("Axis Y") * currentLookSensitivity;
            yRotation += Input.GetAxis("Axis X") * currentLookSensitivity;

            //Movement
            forwardSpeed = Input.GetAxis("Vertical");
            sideSpeed = Input.GetAxis("Horizontal");

        }

        //Clamps the xRotation so the player can't look directly up or directly down.
        xRotation = Mathf.Clamp(xRotation, -upDownRange, upDownRange);

        //Smooths the camera's X and Y rotations based on the velocity and smoothTime.
        currentXRotation = Mathf.SmoothDamp(currentXRotation, xRotation, ref xRotationV, rotationSmoothTime);
        currentYRotation = Mathf.SmoothDamp(currentYRotation, yRotation, ref yRotationV, rotationSmoothTime);

        //Rotates the gameObject to face the camera's Y rotation based on the player's input.
        transform.rotation = Quaternion.Euler(0, currentYRotation, 0);

        //Rotates the camera's X rotation based on the player's input.
        playerMainCamera.transform.localRotation = Quaternion.Euler(currentXRotation, 0, 0);


        RaycastHit hit;
        Debug.DrawLine(transform.position + new Vector3(0, _characterController.height, 0), transform.position + new Vector3(0, _characterController.height, 0) + Vector3.up * jumpHeightDetection);
        if (Input.GetButtonDown("Jump") && canControl && _characterController.isGrounded) {
            //Prevents the player from jumping while an object is above their head.
            if (!Physics.Raycast(transform.position + new Vector3(0,_characterController.height, 0), Vector3.up, out hit, jumpHeightDetection)) {
                verticalVelocity = jumpSpeed;
            }
        //Handles gravity.
        } else if (!_characterController.isGrounded) {
            verticalVelocity += Physics.gravity.y * Time.deltaTime;
        }

        //Creates the vector that will be used to tell the CharacterController where to move.
        Vector3 speed = new Vector3(sideSpeed * moveSpeed * sideSpeedMultiplier, verticalVelocity, forwardSpeed * moveSpeed);
        /*
        Modifies the speed variable to make it relative to the gameObjects rotation. 
        (Aka telling it to move forward will make it move forward)
        */
        speed = transform.rotation * speed;

        //Uses all prior information to move the player.
        _characterController.Move(speed * Time.deltaTime);

        _animator.SetFloat("Blend", _characterController.velocity.sqrMagnitude * moveSpeed);
        #endregion
    }

    public void SetRotation (float yRot) {
        currentYRotation = yRot;
        yRotation = yRot;
    }

    public void SetControl (bool control) {
        canControl = control;
    }

    public bool GetControl() {
        return canControl;
    }

}
