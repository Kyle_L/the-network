﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Customization")]
public class PlayerCustomizationPreset : ScriptableObject {

    [Tooltip("The mesh that will replace the current active mesh on the player.")]
    public Mesh mesh;
    [Tooltip("The materials that will replace the materials currently on the mesh. If there are more materials than the mesh uses then they will not show up.")]
    public Material[] materials;
	
}
