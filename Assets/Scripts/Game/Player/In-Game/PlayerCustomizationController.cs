﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerCustomizationController : NetworkBehaviour {

    [SyncVar] private int myMeshNum;

    public PlayerCustomizationPreset[] presets;
    public SkinnedMeshRenderer playerMesh;

    public override void OnStartLocalPlayer() {
        base.OnStartLocalPlayer();
        myMeshNum = -1 + (int) MyLobbyPlayer.localPlayer.myCharacterEnumIndex;
    }

    public void Start () {
        ChangeMesh(myMeshNum);
    }

    public void Update () {
        if (!isLocalPlayer) return;

        //if (Input.GetButtonDown("Fire3")) ChangeMesh((myMeshNum + 1) % 4);
    }

    public void ChangeMesh(int meshNum) {
        SetMesh(meshNum);
        CmdChangeMesh(meshNum);
    }

    [Command]
    private void CmdChangeMesh(int meshNum) {
        myMeshNum = meshNum;
        RpcChangeMesh(meshNum);
    }

    [ClientRpc]
    private void RpcChangeMesh(int n) {
        if (isLocalPlayer) return;
        SetMesh(n);
    }

    private void SetMesh (int meshNum) {
        playerMesh.sharedMesh = presets[meshNum].mesh;
        playerMesh.sharedMaterials = presets[meshNum].materials;
    }

}
