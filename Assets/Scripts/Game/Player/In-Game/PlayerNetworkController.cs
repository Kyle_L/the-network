﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Text.RegularExpressions;

public class PlayerNetworkController : NetworkBehaviour {

    [SyncVar] public string myName;
    public Text t;

    private void Start () {
        if (!isLocalPlayer) {
            t.text = myName;
            return;
        } else {
            t.enabled = false;
        }
        string name = MyLobbyPlayer.localPlayer.playerName;
        ChangeName(name);
    }

    public void ChangeName(string aName) {

        aName = StringChecker.RemoveRichText(aName);

        if (aName.Length <= 0) return;

        t.text = aName;
        CmdChangeName(aName);
        ////Doesn't work at this time.
        //UNETPlayerManager.instance.UpdatePlayerName(aName);
    }

    [Command]
    private void CmdChangeName(string aName) {
        myName = aName;
        RpcChangeName(myName);
    }

    [ClientRpc]
    private void RpcChangeName(string name) {
        t.text = name;
    }

    public void RequestAuthority(GameObject gameObject) {
        CmdRequestAuthority(gameObject.GetComponent<NetworkIdentity>());
    }

    [Command]
    public void CmdRequestAuthority(NetworkIdentity net) {
       
        if (net.clientAuthorityOwner != null) net.RemoveClientAuthority(net.clientAuthorityOwner);
        net.AssignClientAuthority(connectionToClient);
    }
}
