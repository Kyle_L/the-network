﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerCameraController : NetworkBehaviour {

    public Camera playerCamera;

	private void Start () {
        if (!isLocalPlayer) {
            playerCamera.gameObject.SetActive(false);
            return;
        }

        playerCamera.gameObject.SetActive(false);
        CameraManager.instance.SetCameraFade(playerCamera);
    }
	
	void Update () {
        if (!isLocalPlayer) return;

        RaycastHit hit;
        Debug.DrawRay(playerCamera.transform.position, playerCamera.transform.forward, Color.red);
        if (Input.GetButtonDown("Fire1")) {
            Physics.Raycast(playerCamera.transform.position, playerCamera.transform.forward, out hit, 10);
            //if (hit.transform != null && hit.transform.gameObject.GetComponent<PuzzleSpinPiece>() != null) {
            //    hit.transform.gameObject.GetComponent<PuzzleSpinPiece>().Increment();
            //}
            if (hit.transform != null && hit.transform.gameObject.GetComponent<Openable>() != null) {
                hit.transform.gameObject.GetComponent<Openable>().Interact();
            }
        } else if (Input.GetButtonDown("Fire2")) {
            Physics.Raycast(playerCamera.transform.position, playerCamera.transform.forward, out hit, 10);
            if (hit.transform != null && hit.transform.gameObject.GetComponent<PuzzleSpinPiece>() != null) {
                hit.transform.gameObject.GetComponent<PuzzleSpinPiece>().Decrement();
            }
        }
    }
}
