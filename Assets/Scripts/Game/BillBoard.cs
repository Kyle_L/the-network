﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class BillBoard : NetworkBehaviour {

    public Text text;
    public PlayerControllerMain player;

    void Update () {
        if (isLocalPlayer) return;
        text.transform.LookAt(PlayerControllerMain.instance.playerMovementController.playerMainCamera.transform);
        text.transform.Rotate(0, 180, 0);
	}

    public void Start () {
        //text.text = player.myName;
    }

    [Command]
    public void CmdOnNameChange() {
        //text.text = player.mname;
        RpcChange();
    }

    [ClientRpc]
    public void RpcChange () {
        text.text = player.playerNetworkController.myName;
    }
}
