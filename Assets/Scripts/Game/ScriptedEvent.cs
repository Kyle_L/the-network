﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The class for all scripted events.
/// </summary>
public abstract class ScriptedEvent : MonoBehaviour {

    /// <summary>
    /// Tells the scripted event to start. (Play music, start cutscene, etc.)
    /// </summary>
    public abstract void Play();

    /// <summary>
    /// Tells the scripted event to stop. (Stop music, stop cutscene, etc.)
    /// </summary>
    public abstract void Stop();

}
