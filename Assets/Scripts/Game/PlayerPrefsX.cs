﻿using UnityEngine;
using System.Collections;

/// <summary>
/// A second PlayerPrefs class thats adds a number of methods not included in the original class.
/// </summary>
public class PlayerPrefsX : MonoBehaviour {

    public static void SetBool(string name, bool booleanValue) {
		PlayerPrefs.SetInt(name, booleanValue ? 1 : 0);
	}


	public static bool GetBool(string name)  {
		return PlayerPrefs.GetInt(name) == 1 ? true : false;

	}
		
}