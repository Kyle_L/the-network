﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptedEvent_Shutdoor : ScriptedEvent {

    public bool doesOpenDoor = true;
    public Openable door;

    public override void Play() {
        door.ChangeOpenableState(doesOpenDoor);
    }

    public override void Stop() {
        throw new System.NotImplementedException();
    }

}
