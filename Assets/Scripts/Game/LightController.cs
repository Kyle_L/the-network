﻿using UnityEngine;
using System.Collections;

public class LightController : MonoBehaviour {


    public bool doesFlicker;
    public float flickerSpeed = 0.5f;
    public float currentIntensity;
    public float minFlickerIntensity = 0.5f;
    public float maxFlickerIntensity = 0.75f;

    Light lt;

	private void Awake () {
        gameObject.name += "_Light";
        lt = gameObject.GetComponent<Light>();
        StartCoroutine(Flicker(flickerSpeed));
	}

    public void changeIntensity (float aIntensity) {
        lt.intensity = aIntensity;
    }

    public void turnLightTo(bool doesTurnLightOn) {
        lt.enabled = doesTurnLightOn;
    }

    public void turnLightToOpposite() {
        lt.enabled = !lt.enabled;
    }

    IEnumerator Flicker(float flickerDelay = 0.1f) {
        while (doesFlicker) {
            currentIntensity = Random.Range(minFlickerIntensity, maxFlickerIntensity);
            while (Mathf.Abs(lt.intensity - currentIntensity) > 0.01f) {
                lt.intensity = Mathf.Lerp(lt.intensity, currentIntensity, flickerDelay * Time.deltaTime);
                yield return null;
            }
        }
    }
}
