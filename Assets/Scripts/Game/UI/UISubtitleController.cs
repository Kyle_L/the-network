﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Text.RegularExpressions;

public class UISubtitleController : MonoBehaviour {

    public GameObject subtitleUI;
    private Text subtitleTextUI;
    private string subtitleString;

    public char scriptedEventCharacter;

    const float AVERAGE_WORDS_PER_MINUTE = 115;
    const float WORDS_PER_MINUTE_MULTIPLIER_CHAR_TIME = 0.0325f;
    const float PERIOD_MULTIPLIER = 15f;
    const float COMMA_MULTIPLIER = 10f;
    const float LINE_MULTIPLIER = 15f;

    void Awake() {
        subtitleTextUI = subtitleUI.GetComponent<Text>();
    }

    public void SetNewText(string text, Color objectColor, float defaultDelay = 0) {
        StopAllCoroutines();

        subtitleString = text;

        StartCoroutine(TypeText(subtitleString));
    }

    IEnumerator TypeText(string text) {
        float wordPerMinute = AVERAGE_WORDS_PER_MINUTE / 60 * WORDS_PER_MINUTE_MULTIPLIER_CHAR_TIME;

        string subtitleText = "";

        string[] lines = Regex.Split(text, @"(?=\n)");

        foreach (string line in lines) {
            string textBug;
            char[] chars = line.ToCharArray();

            for (int i = 0; i < line.Length; i++) {
                textBug = line.Substring(i, 1);
                subtitleText = subtitleText + textBug;
                subtitleTextUI.text = subtitleText;

                float waitTime = 0;
                switch (chars[i]) {
                    case '.':
                        waitTime = wordPerMinute * PERIOD_MULTIPLIER;
                        break;
                    case ',':
                        waitTime = wordPerMinute * COMMA_MULTIPLIER;
                        break;
                    default:
                        waitTime = wordPerMinute;
                        break;

                }

                yield return new WaitForSeconds(waitTime);
            }
            yield return new WaitForSeconds(wordPerMinute * LINE_MULTIPLIER);
        }


    }


}