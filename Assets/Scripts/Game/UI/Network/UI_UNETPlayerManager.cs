﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_UNETPlayerManager : MonoBehaviour {

    public Text[] playerNameTexts; 

    private void Update () {
        UpdatePlayerList();
    }

    public void UpdatePlayerList() {
        for (int i = 0; i < playerNameTexts.Length; i++) {
            if (i < LobbyPlayerManager.instance.GetSize()) {
                playerNameTexts[i].text = LobbyPlayerManager.instance.GetLobbyPlayer(i).playerName;
                //playerTexts[i].color = pm.GetPlayerColor(i);
            } else {
                playerNameTexts[i].text = "";
            }

        }
    }

}
