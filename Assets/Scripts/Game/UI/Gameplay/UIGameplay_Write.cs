﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIGameplay_Write : MonoBehaviour {

    bool isActive = false;

    public GameObject inputPrefab;

    public void Update() {

        if (isActive) {
            if (Input.GetButtonDown("Fire1")) {
                GameObject temp = (GameObject) Instantiate(inputPrefab, inputPrefab.transform.parent);
                RectTransform rect = temp.GetComponent<RectTransform>();
                //Logically I can't think why 4 works, but it does...
                rect.position = new Vector2(Input.mousePosition.x + rect.rect.width/4, Input.mousePosition.y);
                temp.GetComponent<InputField>().Select();
                isActive = false;
             }
        }

    }

    public void SetActive() {
        isActive = true;
    }

    public void SetInactive() {
        isActive = false;
    }

}
