﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;
using UnityEngine.EventSystems;
using System.Text.RegularExpressions;
using System;

public class UNETChat : Chat {
    /// <summary>
    /// Because of the way this object handles information there should only ever be one
    /// in the game at a single instance. The class handles the assignment of this variable automatically on Awake.
    /// </summary>
    public static UNETChat instance;

    //just a random number
    private const short chatMessageChannel = 131;
    private const short anoucementMessageChannel = 132;

    public string playerJoinString = "A player joined the game.";
    public Color playerJoinColor = Color.blue;
    public string playerEnterStringPostfix = "entered the game.";
    public Color playerEnterColor = Color.yellow;
    public string playerLeaveStringPostfix = "left the game.";
    public Color playerLeaveColor = Color.magenta;

    private void Awake () {
        #region Enforces Singleton Pattern.
        //Check if instance already exists
        if (instance == null) {
            //if not, set instance to this	
            instance = this;
        }
        //If instance already exists and it's not this:
        else if (instance != this) {
            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a MyNetworkManager.
            Destroy(gameObject);
        }
        #endregion
    }

    public void SetUp () {
        Clear();

        //Doesn't initiate the chat if there is no client.
        if (MyNetworkManager.singleton.client == null) return;

        //if the client is also the server
        if (NetworkServer.active) {
            //registering the server handler
            NetworkServer.RegisterHandler(chatMessageChannel, ServerReceiveMessage);
            NetworkServer.RegisterHandler(anoucementMessageChannel, ServerReceiveAnoucement);

            LobbyPlayerManager.instance.PlayerAdded += new LobbyPlayerManager.PlayerAddedDelegate(JoinAnoucement);
            LobbyPlayerManager.instance.PlayerReady += new LobbyPlayerManager.PlayerReadyDelegate(EnterAnoucement);
            LobbyPlayerManager.instance.PlayerRemoved += new LobbyPlayerManager.PlayerRemovedDelegate(LeaveAnoucement);
        }

        //registering the client handler
        MyNetworkManager.singleton.client.RegisterHandler(chatMessageChannel, ReceiveMessage);
    }

    private void ReceiveMessage(NetworkMessage message) {
        //reading message
        string text = message.ReadMessage<StringMessage>().value;

        AddMessage(text);
    }

    private void ServerReceiveMessage(NetworkMessage message) {
        StringMessage myMessage = new StringMessage();
        //we are using the connectionId as player name only to exemplify
        myMessage.value = LobbyPlayerManager.instance.GetLobbyPlayer(message.conn.connectionId.ToString()).playerName + " : " + message.ReadMessage<StringMessage>().value;

        //sending to all connected clients
        NetworkServer.SendToAll(chatMessageChannel, myMessage);
    }

    public override void SendMessage(UnityEngine.UI.InputField input) {
        SendMessage(input.text);
    }

    public override void SendMessage(string input) {
        StringMessage myMessage = new StringMessage();
        /*
        getting the value of the input && and ensuring it
        won't screw up the richtext of the chat box if the
        player types in a something with richtext tags.
        */
        myMessage.value = StringChecker.RemoveRichText(input);

        //sends to server if the string is not empty and/or just spaces.
        if (myMessage.value.Trim().Length > 0) MyNetworkManager.singleton.client.Send(chatMessageChannel, myMessage);
    }

    public void SendAnoucement (string input, Color color) {
        StringMessage myMessage = new StringMessage();

        input = StringChecker.RemoveRichText(input);
        input = "<color=#" + CustomFunctions.ColorToHex(color) + ">" + input + "</color>";

        myMessage.value = input;

        if (myMessage.value.Trim().Length > 0) NetworkManager.singleton.client.Send(anoucementMessageChannel, myMessage);
    }

    private void ServerReceiveAnoucement(NetworkMessage message) {
        StringMessage myMessage = new StringMessage();
        //we are using the connectionId as player name only to exemplify
        myMessage.value = message.ReadMessage<StringMessage>().value;

        //sending to all connected clients
        NetworkServer.SendToAll(chatMessageChannel, myMessage);
    }

    public void JoinAnoucement(MyLobbyPlayer player) {
        SendAnoucement(playerJoinString, playerJoinColor);
    }

    public void EnterAnoucement (MyLobbyPlayer player) {
        SendAnoucement(player.playerName + " " + playerEnterStringPostfix, playerEnterColor);
    }

    public void LeaveAnoucement (MyLobbyPlayer player) {
        SendAnoucement(player.playerName + " " + playerLeaveStringPostfix, playerLeaveColor);

    }
}