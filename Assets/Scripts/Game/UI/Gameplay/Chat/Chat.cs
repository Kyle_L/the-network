﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Chat : MonoBehaviour {
    [SerializeField]
    private Text container;
    [SerializeField]
    private ScrollRect rect;
    [SerializeField]
    private InputField field;
    [SerializeField]
    private GameObject ui;

    internal void AddMessage(string message) {
        container.text += "\n" + message;

        //just a hack to jump a frame and scrolldown the chat
        Invoke("ScrollDown", .1f);
    }

    public virtual void SendMessage(InputField input) { }

    public virtual new void SendMessage(string input) { }

    private void Update() {

        //THIS SHOULD NOT BE HANDLED BY THE CHAT!
        //SHOULD BE HANDLED IN A FUTURE UI CANVAS CONTROLLER.
        if (Input.GetButtonDown("Submit")) {
                if (field.text.Trim() != "") SendMessage(field);
                field.text = "";
        }

    }

    private void ScrollDown() {
        rect.verticalNormalizedPosition = 0;
        if (rect != null && rect.verticalScrollbar != null)
            rect.verticalScrollbar.value = 0;
    }

    public void Clear () {
        container.text = "";
    }
}