﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UICrossHairController : MonoBehaviour {
    public static UICrossHairController instance;

    public CrosshairStates defaultState;
    public enum CrosshairStates { None, Neutral, Interacting}

    public Image crosshair;
    public Sprite neutralCrosshair;
    public Sprite interactingCrosshair;

    private void Awake() {
        if (instance == null) {
            //if not, set instance to this
            instance = this;
        }
        //If instance already exists and it's not this:
        else if (instance != this) {
            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a UICrossHairController.
            Destroy(gameObject);
            return;
        }
        SetCrosshair(CrosshairStates.Neutral);

    }

    public void SetCrosshair (CrosshairStates aState) {
        if (aState == CrosshairStates.None) {
            crosshair.gameObject.SetActive(false);
        } else {
            crosshair.gameObject.SetActive(true);
            switch (aState) {
                case CrosshairStates.Neutral:
                    crosshair.sprite = neutralCrosshair;
                    break;
                case CrosshairStates.Interacting:
                    crosshair.sprite = interactingCrosshair;
                    break;
            }
        }

    }

}
