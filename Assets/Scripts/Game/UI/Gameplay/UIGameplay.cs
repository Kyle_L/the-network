﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIGameplay : MonoBehaviour {

    [SerializeField]
    private string myName;
    [SerializeField]
    [Tooltip("The GameObject that is selected by the event system when the ui is opened.")]
    public GameObject eventSystemSelectObject;
    public GameObject hideGameObject;
    public bool showMouse;
    public bool disablePlayerControl = true;

    /// <summary>
    /// Returns the name of the menu.
    /// </summary>
    /// <returns></returns>
    public string GetName() {
        return myName;
    }

}
