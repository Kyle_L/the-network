﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIGameplay_Paint : MonoBehaviour {

    bool isActive = false;

    Texture2D texture;
    RectTransform rectT;
    Canvas c;

    int startX;
    int startY;
    int endX;
    int endY;

    public int penSize = 2;
    private Color penColor = Color.black;

    public Color[] penColors;

    public float textureScale = 2;

    void Start () {
        c = GetComponentInParent<Canvas>();
        rectT = GetComponent<RectTransform>();
        texture = new Texture2D((int)(rectT.rect.width * textureScale), (int)(rectT.rect.height * textureScale));
        texture.wrapMode = TextureWrapMode.Clamp;

        GetComponent<RawImage>().texture = texture;

        Clear();
        texture.Apply();
    }

    public void Update() {

        if (isActive) {

            /*Do you believe in magic?
            Calculates the X and Y pixel coordinates of where the the mouse is on the texture.*/
            int xCoordinate = (int)(((Input.mousePosition.x - Screen.width / 2 + (rectT.rect.width * c.scaleFactor) / 2) / c.scaleFactor) * (texture.width / rectT.rect.width));
            int yCoordinate = (int)(((Input.mousePosition.y - Screen.height / 2 + (rectT.rect.height * c.scaleFactor) / 2) / c.scaleFactor) * (texture.height / rectT.rect.height));

            /* When holding left click or "Fire1" draws a line between
             * the last drawn point and the current point your mouse
             * is hovering above. */
            if (Input.GetButton("Fire1")) {
                //If you just start drawing, sets the first point.
                if (startX == 0 && startY == 0) {
                    startX = xCoordinate;
                    startY = yCoordinate;
                //Else, draws a line between the last point and current.
                } else {
                    endX = xCoordinate;
                    endY = yCoordinate;
                    Line(texture, startX, startY, endX, endY, penSize, penColor);
                    startX = endX;
                    startY = endY;
                }
            }

            //Stops drawing
            if (Input.GetButtonUp("Fire1")) {
                startX = 0;
                startY = 0;
            }

        }

    }

    /// <summary>
    /// Activates the ability to paint.
    /// </summary>
    public void SetActive () {
        isActive = true;
    }

    /// <summary>
    /// Disables the ability to paint.
    /// </summary>
    public void SetInactive () {
        isActive = false;
    }

    #region Drawing Methods //These methods are used for drawing things on the texture.

    /// <summary>
    /// Paints the whole texture with a color.
    /// </summary>
    /// <param name="tex"></param>
    /// <param name="color"></param>
    public void Fill (Texture2D tex, Color color) {
        Color[] tempArray = tex.GetPixels();
        for (int i = 0; i < tempArray.Length; i++) {
            tempArray[i] = color;
        }
        tex.SetPixels(tempArray);
        texture.Apply();
    }

    /// <summary>
    /// Removes all color from a texture by setting it to Color.clear.
    /// </summary>
    public void Clear() {
        Fill(texture, Color.clear);
    }

    /// <summary>
    /// Draws a filled in cirlce.
    /// </summary>
    /// <param name="tex"></param>
    /// <param name="cx">The x position in pixels.</param>
    /// <param name="cy">The y position in pixels.</param>
    /// <param name="r">The radius in pixels.</param>
    /// <param name="col">The color of the circle.</param>
    public void Circle(Texture2D tex, int cx, int cy, int r, Color col) {
        for (int y = -r; y <= r; y++)
            for (int x = -r; x <= r; x++)
                if (x * x + y * y <= r * r)
                    tex.SetPixel((cx + x), (cy + y), col);
    }

    #region Line

    /// <summary>
    /// Draws a line.
    /// </summary>
    /// <param name="tex"></param>
    /// <param name="start">The start position in pixels</param>
    /// <param name="end">The end position in pixels</param>
    /// <param name="width">The width of the line.</param>
    /// <param name="col">The color of the line.</param>
    public void Line(Texture2D tex, Vector2 start, Vector2 end, int width, Color col) {
        Line(tex, (int)start.x, (int)start.y, (int)end.x, (int)end.y, width, col);
    }

    /// <summary>
    /// Draws a line.
    /// </summary>
    /// <param name="tex"></param>
    /// <param name="startX">The start X position of the line.</param>
    /// <param name="startY">The start Y position of the line.</param>
    /// <param name="endX">The end X position of the line.</param>
    /// <param name="endY">The end Y position of the line.</param>
    /// <param name="width">Ther width of the line.</param>
    /// <param name="col">The color of the line.</param>
    public void Line (Texture2D tex, int startX, int startY, int endX, int endY, int width, Color col) {
        IEnumerable<Vector2> points = GetPointsOnLine(startX, startY, endX, endY);
        foreach (Vector2 v in points) {
            Circle(tex, (int)v.x, (int)v.y, width, col);
        }
        texture.Apply();
    }

    /// <summary>
    /// Finds all the points (pixels) on a line.
    /// </summary>
    /// <param name="startX">The start X position of the line.</param>
    /// <param name="startY">The start Y position of the line.</param>
    /// <param name="endX">The end X position of the line.</param>
    /// <param name="endY">The end Y position of the line.</param>
    /// <returns></returns>
    public static IEnumerable<Vector2> GetPointsOnLine(int startX, int startY, int endX, int endY) {
        bool steep = Mathf.Abs(endY - startY) > Mathf.Abs(endX - startX);
        if (steep) {
            int t;
            t = startX; // swap x0 and y0
            startX = startY;
            startY = t;
            t = endX; // swap x1 and y1
            endX = endY;
            endY = t;
        }
        if (startX > endX) {
            int t;
            t = startX; // swap x0 and x1
            startX = endX;
            endX = t;
            t = startY; // swap y0 and y1
            startY = endY;
            endY = t;
        }
        int dx = endX - startX;
        int dy = Mathf.Abs(endY - startY);
        int error = dx / 2;
        int ystep = (startY < endY) ? 1 : -1;
        int y = startY;
        for (int x = startX; x <= endX; x++) {
            yield return new Vector2((steep ? y : x), (steep ? x : y));
            error = error - dy;
            if (error < 0) {
                y += ystep;
                error += dx;
            }
        }
        yield break;
    }
    #endregion

    #endregion

    #region Modification Methods //These methods change how the brush draws.

    #region ChangeColor

    /// <summary>
    /// Changes the color of the pen.
    /// </summary>
    /// <param name="color">The new color of the pen.</param>
    public void ChangeColor (Color color) {
        penColor = color;
    }

    /// <summary>
    /// Changes the color of the pen.
    /// </summary>
    /// <param name="color">The index of a color in the penColors array. If the index is not valid the color will change to black.</param>
    public void ChangeColor(int color) {
        if (color < 0 || color >= penColors.Length) {
            penColor = Color.black;
        } else {
            penColor = penColors[color];
        }
    }

    #endregion 

    /// <summary>
    /// Changes the size of the brush.
    /// </summary>
    /// <param name="size"></param>
    public void ChangeSize(float size) {
        penSize = (int)size;
    }

    #endregion

}
