﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UIGameplayController : MonoBehaviour {
    public static UIGameplayController instance;

    [SerializeField]
    private UIGameplay chatUI;
    [SerializeField]
    private UIGameplay notepadUI;
    [SerializeField]
    private UIGameplay notepadUI2;

    public enum Gameplays { NoUI, Misc, Chat, Notepad, Notepad2 };
    [SerializeField]
    private Gameplays startMenu = Gameplays.NoUI;
    private Gameplays currentMenu;
    private Stack<UIGameplay> uiHistory = new Stack<UIGameplay>();

    private void Awake() {
        if (instance == null) {
            //if not, set instance to this
            instance = this;
        }
        //If instance already exists and it's not this:
        else if (instance != this) {
            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a UIMenuController.
            Destroy(gameObject);
            return;
        }

        SetMenu(startMenu);
    }

    /// <summary>
    /// Returns to a previously active menu. If no there is no previous menu, then just shut the current menu.
    /// </summary>
    public void Back() {
        if (uiHistory.Count <= 1) {
            SetMenu(Gameplays.NoUI);
        } else {
            uiHistory.Pop().hideGameObject.SetActive(false);
            SetMenu(uiHistory.Peek(), false);
        }
    }

    /// <summary>
    /// Opens the pause menu.
    /// </summary>
    public void ChatUI() {
        SetMenu(Gameplays.Chat);
    }

    public void NotepadUI() {
        SetMenu(Gameplays.Notepad);
    }

    public void NotepadUI2() {
        SetMenu(Gameplays.Notepad2);
    }

    /// <summary>
    /// Closes the current menu.
    /// </summary>
    public void CloseGameplayUI() {
        SetMenu(Gameplays.NoUI);
    }

    ////public void HideCurrentMenu () {
    ////    EventSystem.current.SetSelectedGameObject(null);
    ////    uiHistory.Peek().hideGameObject.SetActive(false);
    ////}

    //public void UnHideCurrentMenu () {
    //    EventSystem.current.SetSelectedGameObject(null);
    //    EventSystem.current.SetSelectedGameObject(uiHistory.Peek().eventSystemSelectObject);
    //    uiHistory.Peek().gameObject.SetActive(true);
    //}

    /// <summary>
    /// Sets a menu to active.
    /// </summary>
    /// <param name="aMenu">The menu you would like to set active. Please note see the overloaded method to send a Menu object rather than enumerated type.</param>
    public void SetMenu(Gameplays aMenu) {
        switch (aMenu) {
            case Gameplays.NoUI:
                SetMenu(null);
                break;
            case Gameplays.Chat:
                SetMenu(chatUI);
                break;
            case Gameplays.Notepad:
                SetMenu(notepadUI);
                break;
            case Gameplays.Notepad2:
            SetMenu(notepadUI2);
                break;
            default:
                SetMenu(null);
                break;
        }
    }

    /// <summary>
    /// Sets a menu to active. 
    /// Please Note.
    /// </summary>
    /// <param name="aGameplayUI">The menu you would like to set active.</param>
    public void SetMenu(UIGameplay aGameplayUI) {
        SetMenu(aGameplayUI, true);
    }

    /// <summary>
    /// Sets a menu to active. 
    /// Please Note.
    /// </summary>
    /// <param name="aGameplayUI">The menu you would like to set active.</param>
    public void SetMenu(UIGameplay aGameplayUI, bool addToHistory) {
        if (uiHistory.Count > 0) uiHistory.Peek().hideGameObject.SetActive(false);

        if (aGameplayUI == null) {
            uiHistory.Clear();
            EventSystem.current.SetSelectedGameObject(null);
            if (MouseStateController.instance != null) MouseStateController.instance.SetMouseState(false);
            if (PlayerControllerMain.instance != null) PlayerControllerMain.instance.SetControl(true);
        } else {
            aGameplayUI.hideGameObject.SetActive(true);
            EventSystem.current.SetSelectedGameObject(null);
            EventSystem.current.SetSelectedGameObject(aGameplayUI.eventSystemSelectObject);
            if (addToHistory) uiHistory.Push(aGameplayUI);
            if (aGameplayUI.showMouse && MouseStateController.instance != null) MouseStateController.instance.SetMouseState(true);
            if (PlayerControllerMain.instance != null) PlayerControllerMain.instance.SetControl(!aGameplayUI.disablePlayerControl);
        }
    }

    /// <summary>
    /// Returns the current active menu.
    /// </summary>
    /// <returns>The active menu, null if no menu is active.</returns>
    public UIGameplay GetCurrentMenu() {
        return (uiHistory.Count > 0) ? uiHistory.Peek() : null;
    }

    /// <summary>
    /// Returns the current active menu.
    /// </summary>
    /// <returns>The active menu.</returns>
    public Gameplays GetCurrentMenuEnum() {
        UIGameplay current = GetCurrentMenu();
        if (current == null) {
            return Gameplays.NoUI;
        } else if (current == chatUI) {
            return Gameplays.Chat;
        } else if (current == notepadUI) {
            return Gameplays.Notepad;
        } else if (current == notepadUI2) {
            return Gameplays.Notepad2;
        } else {
            return Gameplays.Misc;
        }
    }

    /// <summary>
    /// Returns whether a menu is currently active.
    /// </summary>
    /// <returns>True if a menu is active, false otherwise.</returns>
    public bool GetMenuState() {
        return uiHistory.Count > 0;
    }

}
