﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIFadeTransitionController : MonoBehaviour {
    public static UIFadeTransitionController instance;

    public GameObject fadeUI;
    public GameObject fadeUIText;

    private CanvasGroup fadeCanvasGroup;

    [HideInInspector]
    public bool isFading = false;

    public float defaultFadeOutSmoothSpeed = 1f;
    public float defaultFadeInSmoothSpeed = 1f;
    public float defaultGameplayFadeSmoothSpeed = 5f;

    public float introWaitDelay;

    Coroutine uiTextFadeCoroutine;
    Coroutine uiFadeCoroutine;

    private void Awake () {
        #region Enforces Singleton Pattern.
        //Check if instance already exists
        if (instance == null) {
            //if not, set instance to this	
            instance = this;
        }
        //If instance already exists and it's not this:
        else if (instance != this) {
            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a MyNetworkManager.
            Destroy(gameObject);
        }
        #endregion

        fadeCanvasGroup = fadeUI.GetComponent<CanvasGroup>();
        fadeCanvasGroup.alpha = 1;
        fadeCanvasGroup.gameObject.SetActive(true);
        //FadeUIOutDelay(0.75f, 2.5f, false);
	}

    public void FadeUIIn(float smoothValue = 0.75f, bool startAtCurrentAlpha = true) {
 

        isFading = true;
        if (uiFadeCoroutine != null) StopCoroutine(uiFadeCoroutine);
        fadeUI.SetActive(true);
        if (startAtCurrentAlpha) {
            uiFadeCoroutine = StartCoroutine(FadeObjectIEnumerator(fadeUI, 1,smoothValue, 0));
        } else {
            uiFadeCoroutine = StartCoroutine(FadeObjectIEnumerator(fadeUI, 1, smoothValue, 0, 0));
        }
    }
    
	public void FadeUIOut (float smoothValue = 0.75f, bool startAtCurrentAlpha = true) {
        isFading = true;
        if (uiFadeCoroutine != null) StopCoroutine(uiFadeCoroutine);
        fadeUI.SetActive(true);
        if (startAtCurrentAlpha) {
            uiFadeCoroutine = StartCoroutine(FadeObjectIEnumerator(fadeUI, 0, smoothValue, 0));
        } else {
            uiFadeCoroutine = StartCoroutine(FadeObjectIEnumerator(fadeUI, 0, smoothValue, 1, 0));
        }
    }

    public void FadeUIInDelay(float smoothValue = 0.75f, float delay = 0, bool startAtCurrentAlpha = true) {
        isFading = true;
        if (uiFadeCoroutine != null) StopCoroutine(uiFadeCoroutine);
        fadeUI.SetActive(true);
        if (startAtCurrentAlpha) {
            uiFadeCoroutine = StartCoroutine(FadeObjectIEnumerator(fadeUI, 1, smoothValue, delay));
        } else {
            uiFadeCoroutine = StartCoroutine(FadeObjectIEnumerator(fadeUI, 1, smoothValue, 0, delay));
        }
    }

    public void FadeUIOutDelay(float smoothValue, float delay = 0, bool startAtCurrentAlpha = true) {
        isFading = true;
        if (uiFadeCoroutine != null) StopCoroutine(uiFadeCoroutine);
        fadeUI.SetActive(true);
        if (startAtCurrentAlpha) {
            uiFadeCoroutine = StartCoroutine(FadeObjectIEnumerator(fadeUI, 0, smoothValue, delay));
        } else {
            uiFadeCoroutine = StartCoroutine(FadeObjectIEnumerator(fadeUI, 0, smoothValue, 1, delay));
        }
    }

    public void FadeIn (GameObject ui, float smoothValue, bool startAtCurrentAlpha) {
        if (startAtCurrentAlpha) {
            uiFadeCoroutine = StartCoroutine(FadeObjectIEnumerator(ui, 1, smoothValue, 0, 0));
        } else {
            uiFadeCoroutine = StartCoroutine(FadeObjectIEnumerator(ui, 1, smoothValue, 0));
        }
    }

    public void FadeOut (GameObject ui, float smoothValue, bool startAtCurrentAlpha) {
        if (startAtCurrentAlpha) {
            uiFadeCoroutine = StartCoroutine(FadeObjectIEnumerator(ui, 0, smoothValue, 1, 0));
        } else {
            uiFadeCoroutine = StartCoroutine(FadeObjectIEnumerator(ui, 0, smoothValue, 0));
        }
    }

    public Coroutine FadeObject (GameObject gameObject, float aAlpha, float speed, float delay) {
        return StartCoroutine(FadeObjectIEnumerator(gameObject, aAlpha, speed, delay));
    }

    public Coroutine FadeObject(GameObject gameObject, float aAlpha, float speed, float startAlpha, float delay) {
        return StartCoroutine(FadeObjectIEnumerator(gameObject, aAlpha, speed, startAlpha, delay));
    }

    private IEnumerator FadeObjectIEnumerator(GameObject gameObject, float aAlpha, float speed, float delay){
        yield return new WaitForSeconds(delay);
        CanvasGroup cg = gameObject.GetComponent<CanvasGroup>();
        if (!gameObject.activeSelf) gameObject.SetActive(true);
        while (Mathf.Abs(cg.alpha - aAlpha) > 0.001f) {
            cg.alpha = Mathf.Lerp(cg.alpha, aAlpha, speed * Time.unscaledDeltaTime);
            yield return null;
        }
        cg.alpha = aAlpha;
        if (aAlpha == 0) gameObject.SetActive(false);
        isFading = false;
    } 

    private IEnumerator FadeObjectIEnumerator(GameObject gameObject, float aAlpha, float speed, float startAlpha, float delay) {
        CanvasGroup cg = gameObject.GetComponent<CanvasGroup>();
        cg.alpha = startAlpha;
        yield return new WaitForSeconds(delay);
        if (!gameObject.activeSelf) gameObject.SetActive(true);
        while (Mathf.Abs(cg.alpha - aAlpha) > 0.001f) {
            cg.alpha = Mathf.Lerp(cg.alpha, aAlpha, speed * Time.unscaledDeltaTime);
            yield return null;
        }
        cg.alpha = aAlpha;
        if (aAlpha == 0) {
            gameObject.SetActive(false);
        }
        isFading = false;
    }

    public void SetFadeUIText(bool resetWhenFaded, float aAlpha, float speed, float delay) {
        if (uiTextFadeCoroutine != null) StopCoroutine(uiTextFadeCoroutine);
        uiTextFadeCoroutine = FadeObject(fadeUIText, aAlpha, speed, delay);
        if (resetWhenFaded) StartCoroutine(ResetText());
    }

    public void SetFadeUIText(bool resetWhenFaded, float aAlpha, float speed, float startAlpha, float delay) {
        if (uiTextFadeCoroutine != null) StopCoroutine(uiTextFadeCoroutine);
        uiTextFadeCoroutine = FadeObject(fadeUIText, aAlpha, speed, startAlpha, delay);
        if (resetWhenFaded) StartCoroutine(ResetText());
    }

    public void SetFadeUIText (string text, bool resetWhenFaded, float aAlpha, float speed, float delay) {
        fadeUIText.GetComponent<Text>().text = text;
        if (uiTextFadeCoroutine != null) StopCoroutine(uiTextFadeCoroutine);
        uiTextFadeCoroutine = FadeObject(fadeUIText, aAlpha, speed, delay);
        if (resetWhenFaded)  StartCoroutine(ResetText());
    }

    public void SetFadeUIText(string text, bool resetWhenFaded, float aAlpha, float speed, float startAlpha, float delay) {
        fadeUIText.GetComponent<Text>().text = text;
        if (uiTextFadeCoroutine != null) StopCoroutine(uiTextFadeCoroutine);
        uiTextFadeCoroutine = FadeObject(fadeUIText, aAlpha, speed, startAlpha, delay);
        if (resetWhenFaded) StartCoroutine(ResetText());
    }

    private IEnumerator ResetText () {
        while (isFading || fadeCanvasGroup.alpha > 0) {
            yield return null;
        }
        fadeUIText.GetComponent<Text>().text = "";
    }

}
