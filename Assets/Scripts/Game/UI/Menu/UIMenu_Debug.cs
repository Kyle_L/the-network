﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMenu_Debug : MonoBehaviour {

    public Text localIpAddressText;
    public Text externalIpAddressText;

    private void Start () {
        if (localIpAddressText != null) localIpAddressText.text = GetLocalIpAddress();
        if (externalIpAddressText != null) externalIpAddressText.text = GetExternalIpAddress();
    }

    public string GetExternalIpAddress () {
        return "External Ip: " + MyNetworkManager.singleton.GetExternalIpAddress();
    }

    public string GetLocalIpAddress() {
        return "Local Ip: " + MyNetworkManager.singleton.GetLocalIpAddress();
    }

    public void ChangePlayerName (string aName) {
        PlayerControllerMain.instance.playerNetworkController.ChangeName(aName);
    }

    public void ChangePlayerName(InputField input) {
        PlayerControllerMain.instance.playerNetworkController.ChangeName(input.text);
    }

}
