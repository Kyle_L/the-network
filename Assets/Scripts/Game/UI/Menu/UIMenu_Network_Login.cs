﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMenu_Network_Login : MonoBehaviour {

    public Text warningText;

    [Header("Warnings")]
    public string nameAlreadyInUseWarning = "That name is already in use.";
    public string enterANameWarning = "Please enter a name.";
    public string enterAValidNameWarning = "Please enter a valid name";
    public string selectACharacterWarning = "Please select a character";
    public string someoneElseIsUsingCharacterWarning = "That character is already in use.";

    public int minimumSize = 3;

    private void Start () {
        warningText.text = "";
    }

    public void EnterGame () {
        EnterGame(MyLobbyPlayer.localPlayer);
    }

    /// <summary>
    /// Enters the game session.
    /// </summary>
    /// <param name="input"></param>
    public void EnterGame(MyLobbyPlayer player) {
        /*Doesn't let the player enter the game if they don't have a 
        name or a character selected.*/
        if (player.myCharacterEnumIndex <= 0 || player.playerName.Trim().Length < minimumSize) {

            warningText.text = "";
            if (player.playerName.Trim().Length > 0 && StringChecker.RemoveRichText(player.playerName.Trim()).Length < minimumSize) {
                warningText.text += "*" + enterAValidNameWarning + "\n";
            } else if (player.playerName.Trim().Length <= 0) {
                warningText.text += "*" + enterANameWarning + "\n";
            }

            if (player.myCharacterEnumIndex == 0) {
                warningText.text += "*" + selectACharacterWarning + "\n";
            }
            return;
        }

        UIMenuController.instance.SetMenu(UIMenuController.MainMenus.NoMenu);
        MyLobbyPlayer.localPlayer.CmdEnterGame();
    }

}
