﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

/// <summary>
/// The main controller of all the game's UI menus. 
/// Please Note this is a singleton class.
/// </summary>
public class UIMenuController : MonoBehaviour {
    public static UIMenuController instance;

    public enum MainMenus { NoMenu, MainMenu, PauseMenu, LogInMenu, LobbyMenu };
    private Stack<UIMenu> uiHistory = new Stack<UIMenu>();

    [SerializeField]
    private GameObject backgroundUI;
    [SerializeField]
    private UIMenu mainMenuUI;
    [SerializeField]
    private UIMenu pauseMenuUI;
    [SerializeField]
    private UIMenu logInMenuUI;
    [SerializeField]
    private UIMenu lobbyMenuUI;

    private void Awake () {
        if (instance == null) {
            //if not, set instance to this
            instance = this;
        }
        //If instance already exists and it's not this:
        else if (instance != this) {
            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a UIMenuController.
            Destroy(gameObject);
            return;
        }

    }

    private void Update() {

        if (Input.GetButtonDown("Cancel")) {
            bool menuState = UIGameplayController.instance.GetMenuState();
            if (menuState) {
                UIGameplayController.instance.CloseGameplayUI();
            } else {
                menuState = GetMenuState();
                if (!menuState) SetMenu(MainMenus.PauseMenu);
                else if (GetCurrentMenu().GetBackState()) Back();
            }
        }

        if (Input.GetButtonDown("Submit")) {
            if (!UIMenuController.instance.GetMenuState()) {
                if (UIGameplayController.instance.GetCurrentMenuEnum() == UIGameplayController.Gameplays.Chat) {
                    UIGameplayController.instance.CloseGameplayUI();
                } else if (UIGameplayController.instance.GetCurrentMenuEnum() == UIGameplayController.Gameplays.NoUI) {
                    UIGameplayController.instance.ChatUI();
                }
            }
        }

        if (Input.GetButtonDown("Tab2")) {
            if (!UIMenuController.instance.GetMenuState()) {
                if (UIGameplayController.instance.GetCurrentMenuEnum() == UIGameplayController.Gameplays.Notepad2) {
                    UIGameplayController.instance.CloseGameplayUI();
                } else if (UIGameplayController.instance.GetCurrentMenuEnum() == UIGameplayController.Gameplays.NoUI) {
                    UIGameplayController.instance.NotepadUI2();
                }
            }
        }

        if (Input.GetButtonDown("Tab")) {
            if (!UIMenuController.instance.GetMenuState()) {
                if (UIGameplayController.instance.GetCurrentMenuEnum() == UIGameplayController.Gameplays.Notepad) {
                    UIGameplayController.instance.CloseGameplayUI();
                } else if (UIGameplayController.instance.GetCurrentMenuEnum() == UIGameplayController.Gameplays.NoUI) {
                    UIGameplayController.instance.NotepadUI();
                }
            }
        }
    }

    /// <summary>
    /// Returns to a previously active menu. If no there is no previous menu, then just shut the current menu.
    /// </summary>
    public void Back () {
        if (uiHistory.Count <= 1) {
            SetMenu(MainMenus.NoMenu);
        } else {
            uiHistory.Pop().gameObject.SetActive(false);
            SetMenu(uiHistory.Peek(), false);
        }
    }

    /// <summary>
    /// Opens the pause menu.
    /// </summary>
    public void PauseMenu () {
        SetMenu(MainMenus.PauseMenu);
    }

    /// <summary>
    /// Closes the current menu.
    /// </summary>
    public void CloseMenu () {
        SetMenu(MainMenus.NoMenu);
    }

    /// <summary>
    /// Sets a menu to active.
    /// </summary>
    /// <param name="aMenu">The menu you would like to set active. Please note see the overloaded method to send a Menu object rather than enumerated type.</param>
    public void SetMenu (MainMenus aMenu) {
        switch (aMenu) {
            case MainMenus.LobbyMenu:
                SetMenu(lobbyMenuUI);
                break;
            case MainMenus.LogInMenu:
                SetMenu(logInMenuUI);
                break;
            case MainMenus.MainMenu:
                SetMenu(mainMenuUI);
                break;
            case MainMenus.PauseMenu:
                SetMenu(pauseMenuUI);
                break;
            default:
                SetMenu(null);
                break;
        }
    }

    /// <summary>
    /// Sets a menu to active. 
    /// Please Note.
    /// </summary>
    /// <param name="aMenu">The menu you would like to set active.</param>
    public void SetMenu(UIMenu aMenu) {
        SetMenu(aMenu, true);
    }

    /// <summary>
    /// Sets a menu to active. 
    /// Please Note.
    /// </summary>
    /// <param name="aMenu">The menu you would like to set active.</param>
    public void SetMenu(UIMenu aMenu, bool addToHistory) {
        if (uiHistory.Count > 0) uiHistory.Peek().gameObject.SetActive(false);

        if (aMenu == null) {
            uiHistory.Clear();
            backgroundUI.SetActive(false);
            //Should improve.
            if (UICrossHairController.instance != null) UICrossHairController.instance.SetCrosshair(UICrossHairController.CrosshairStates.Neutral);
            if (MouseStateController.instance != null) MouseStateController.instance.SetMouseState(false);
            if (PlayerControllerMain.instance != null) PlayerControllerMain.instance.SetControl(true);
        } else {
            aMenu.gameObject.SetActive(true);
            backgroundUI.SetActive(aMenu.DoesUseMenuBackground());
            if (addToHistory) uiHistory.Push(aMenu);
            //Should improve.
            if (UICrossHairController.instance != null) UICrossHairController.instance.SetCrosshair(UICrossHairController.CrosshairStates.None);
            if (MouseStateController.instance != null) MouseStateController.instance.SetMouseState(true);
            if (PlayerControllerMain.instance != null) PlayerControllerMain.instance.SetControl(false);
        }
    }

    /// <summary>
    /// Returns the current active menu.
    /// </summary>
    /// <returns>The active menu, null if no menu is active.</returns>
    public UIMenu GetCurrentMenu () {
        return (uiHistory.Count > 0) ? uiHistory.Peek() : null;
    }

    /// <summary>
    /// Returns whether a menu is currently active.
    /// </summary>
    /// <returns>True if a menu is active, false otherwise.</returns>
    public bool GetMenuState () {
        return uiHistory.Count > 0;
    }

}
