﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// All player settings related functions needed inside of the menus.
/// </summary>
public class UIMenu_Settings : MonoBehaviour {

    public Slider lookSensitivitySlider;

    private void Start() {
        //lookSensitivitySlider.value = PlayerPrefs.GetFloat("LookSensitivty");
    }

    //Need to come up with a way to set default values.
    public void ChangeLookSensitivity (float aSensitivity) {
        if (PlayerControllerMain.instance != null) PlayerControllerMain.instance.playerSettingsController.ChangeLookSensitivity(aSensitivity);
        else print("Can't change sensitivity because no PlayerControllerMain exists.");
        
    }

    /// <summary>
    /// Changes the quality level of the game.
    /// Please Note: Starts at index 0.
    /// </summary>
    /// <param name="quality"></param>
    public void ChangeQualitySettings (int quality) {
        QualitySettings.SetQualityLevel(quality);
    }

}
