﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// All network related functions needed inside of menus.
/// </summary>
public class UIMenu_Network : MonoBehaviour { 

    /// <summary>
    /// Begins hosting a session of the game.
    /// </summary>
    public void Host() {
        MyNetworkManager.singleton.Host();
    }

    /// <summary>
    /// Connects to a instance of a game.
    /// </summary>
    /// <param name="input">The public input IP address of the game being connected to.</param>
    public void Connect(InputField input) {
        MyNetworkManager.singleton.Connect(input);
    }

    /// <summary>
    /// Leaves the current session of the game.
    /// </summary>
    public void Leave() {
        //MyNetworkManager.singleton.Leave();
        MyNetworkManager.singleton.Leave();
    }

    public void Ready() {
        UIMenuController.instance.SetMenu(UIMenuController.MainMenus.NoMenu);
        MyLobbyPlayer.localPlayer.CmdEnterGame();
    }

}