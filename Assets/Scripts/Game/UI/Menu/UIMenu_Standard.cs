﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// All functions that are considered standard and needed inside of menus such as Quit.
/// </summary>
public class UIMenu_Standard : MonoBehaviour {

    /// <summary>
    /// Shuts down the application.
    /// </summary>
	public void Quit () {
        Application.Quit();
    }

}
