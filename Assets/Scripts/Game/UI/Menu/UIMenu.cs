﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Put this componenet on the parent object of menu panels.
/// </summary>
public class UIMenu : MonoBehaviour {

    [SerializeField]
    private string myName;
    [SerializeField]
    private bool useMenuBackground = true;
    [SerializeField]
    private bool canGoBack = true;

    /// <summary>
    /// Returns the name of the menu.
    /// </summary>
    /// <returns></returns>
    public string GetName () {
        return myName;
    }

    /// <summary>
    /// Indicates whether this menu uses the default menu background.
    /// </summary>
    /// <returns></returns>
    public bool DoesUseMenuBackground () {
        return useMenuBackground;
    }

    /// <summary>
    ///Returns whether you are allowed to go back to prior menu from this menu.
    /// </summary>
    /// <returns></returns>
    public bool GetBackState() {
        return canGoBack;
    }

}
