﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMenu_Network_CharacterSelect : MonoBehaviour {

    public Button one;
    public Button two;
    public Button three;
    public Button four;
	
    /// <summary>
    /// Updates all the buttons.
    /// </summary>
	public void UpdateButtons () {
        one.interactable = (LobbyPlayerManager.instance.GetLobbyPlayer(Player.Character.One) != null);
        two.interactable = (LobbyPlayerManager.instance.GetLobbyPlayer(Player.Character.Two) != null);
        three.interactable = (LobbyPlayerManager.instance.GetLobbyPlayer(Player.Character.Three) != null);
        four.interactable = (LobbyPlayerManager.instance.GetLobbyPlayer(Player.Character.Four) != null);

    }

    /// <summary>
    /// Selects a character.
    /// </summary>
    /// <param name="aPlayer"></param>
    public void SelectCharacter(int character) {
        MyLobbyPlayer.localPlayer.SetCharacter((Player.Character)character);
        //MyNetworkManager.singleton.SetDefaultCharacter((PlayerMessage.character) character);
    }

}
