﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trigger_ScriptedEvent : MonoBehaviour {

    public ScriptedEvent scriptedEvent;

    private void OnTriggerEnter(Collider other) {
        scriptedEvent.Play();
    }

}
