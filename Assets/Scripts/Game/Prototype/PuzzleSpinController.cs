﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleSpinController : MonoBehaviour {
    
    [Tooltip("Please ensure answer is the same length as the number of pieces.")]
    [SerializeField]
    private int[] answer;

    [SerializeField]
    public PuzzleSpinPiece[] pieces;

    [SerializeField]
    private Openable[] openable;


    public void IncrementPiece (PuzzleSpinPiece piece) {
        ModifyPiece(piece, 1);
    }

    public void DecrementPiece(PuzzleSpinPiece piece) {
        ModifyPiece(piece, -1);
    }

    private void ModifyPiece(PuzzleSpinPiece piece, int amount) {
        int value = piece.GetValue() + amount;
        value = (((value - 0) % (10 - 0)) + (10 - 0)) % (10 - 0);
        piece.SetValue(value);

        int index = 0;
        for (int i = 0; i < pieces.Length; i++) {
            if (pieces[i] == piece) {
                index = i;
                break;
            }
        }

        bool matches = true;
        for (int i = 0; i < pieces.Length; i++) {
            //if (pieces[i] != piece) {
            //    value = pieces[i].GetValue() + amount * index;
            //    value = (((value - 0) % (10 - 0)) + (10 - 0)) % (10 - 0);
            //    pieces[i].SetValue(value);
            //}
            if (pieces[i].GetValue() != answer[i]) {
                matches = false;
            }
        }

        if (matches) {
            EventGo();
            for (int i = 0; i < pieces.Length; i++) {
                pieces[i].Done();
            }
        }

    }

    private void EventGo () {
        for (int i = 0; i < openable.Length; i++) {
            PlayerControllerMain.instance.playerNetworkController.RequestAuthority(openable[i].gameObject);
            openable[i].ChangeOpenableState();
        }
    }

}
