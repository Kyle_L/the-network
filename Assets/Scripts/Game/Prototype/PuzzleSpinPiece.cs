﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PuzzleSpinPiece : NetworkBehaviour {

    public PuzzleSpinController controller;

    [SerializeField]
    [SyncVar]
    private int myValue;

    private int matIndex = 2;
    [SerializeField]
    private Material doneMaterial;

    [SyncVar]
    private bool isDone;

    Vector3 initRot;
    private float rotationBuffer = 2.5f;
    Coroutine rotate;

    private NetworkIdentity net;

    private void Start () {
        if (isDone) gameObject.GetComponent<Renderer>().materials[matIndex].color = Color.green;

        net = GetComponent<NetworkIdentity>();

        initRot = new Vector3(90, -90, -90);
        transform.localRotation = Quaternion.Euler(new Vector3(initRot.x - 36 * myValue, -90, -90));
    }

	public void SetValue (int value) {
        PlayerControllerMain.instance.playerNetworkController.RequestAuthority(gameObject);
                
        myValue = value;

        Vector3 rotation = new Vector3(initRot.x - 36 * myValue, -90, -90);
        if (rotate != null) StopCoroutine(rotate);
        rotate = StartCoroutine(Rotate(rotation));

        CmdRotate(rotation);
        CmdSet(myValue);
    }

    [Command]
    private void CmdSet(int value) {
        myValue = value;
    }

    public int GetValue () {
        return myValue;
	}

    public void Increment () {
        if (!isDone) controller.IncrementPiece(this);
    }

    public void Decrement() {
        if (!isDone) controller.DecrementPiece(this);
    }

    public void Done() {
        isDone = true;
        CmdDone();
    }

    [Command]
    private void CmdDone() {
        RpcDone();
    }

    [ClientRpc]
    private void RpcDone() {
        gameObject.GetComponent<Renderer>().materials[matIndex].color = Color.green;
    }

    [Command]
    private void CmdRotate (Vector3 target) {
        RpcRotate(target);
    }

    [ClientRpc]
    private void RpcRotate (Vector3 target) {
        if (!hasAuthority)
        RotateObject(target);
    }

    private void RotateObject(Vector3 target) {
        if (net.hasAuthority) return;
        if (rotate != null) StopCoroutine(rotate);
        rotate = StartCoroutine(Rotate(target));
    }

    private IEnumerator Rotate (Vector3 target) {
        while (Vector3.Distance(transform.localRotation.eulerAngles, target) > rotationBuffer) {
            transform.localRotation = Quaternion.Lerp(transform.localRotation, Quaternion.Euler(target), 5 * Time.deltaTime);
            yield return null;
        }
    }
}
