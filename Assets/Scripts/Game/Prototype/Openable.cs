﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[RequireComponent(typeof(Collider))]
[RequireComponent(typeof(AudioSource))]
public class Openable : NetworkBehaviour {

    //If Openable is open
    [Header("Openable Settings")]
    public bool isOpen = false;
    public bool isLocked = false;
    public bool canCloseAfterOpening = true;
    public float smoothSpeed = 2f;

    [Header("Transform")]
    //Rotation
    [Header("Rotation")]
    public bool changeAngle = true;
    private bool isRotationChangingState = false;
    public bool openRelativeToPlayer = true;
    public bool openOppositeToPlayer = true;
    public Vector3 openAngle;
    public Vector3 closeAngle;

    //Postion
    [Header("Position")]
    public bool changePosition = true;
    private bool isPositionChangingState = false;
    public Vector3 openLocalPosition;
    public Vector3 closeLocalPosition;

    //Physics
    private bool isDoorColliding = false;

    private Quaternion targetRotation;
    private Vector3 targetPosition;
    
    protected void Awake () {
        //Sets default targetRotation and uiInteractable.
        if (isLocked) {
            isOpen = false;
            targetRotation = Quaternion.Euler(closeAngle);
        } else if (isOpen) {
            if (changeAngle) targetRotation = Quaternion.Euler(openAngle);
            if (changePosition) targetPosition = openLocalPosition;
        } else {
            targetRotation = Quaternion.Euler(closeAngle);
        }

        //Sets Openable's default rotation.
        if (changeAngle) transform.localRotation = targetRotation;
        if (changePosition) transform.localPosition = targetPosition;
    }

    public void Interact () {
        PlayerControllerMain.instance.playerNetworkController.RequestAuthority(gameObject);
        ChangeOpenableState();
    }

    [Command]
    private void CmdInteract() {
        RpcInteract();
    }

    [ClientRpc]
    private void RpcInteract() {
        if (!hasAuthority)
            ChangeOpenableState();
    }

    public void ChangeOpenableState() {
        if (!isLocked) {
            isOpen = !isOpen;
            StopAllCoroutines();
            if (changeAngle || isDoorColliding) StartCoroutine(RotateOpenable());
            if (changePosition) StartCoroutine(MoveOpenable());

            if (isOpen && !canCloseAfterOpening) gameObject.tag = "Untagged";
        } else {
            isOpen = false;
        }

    }

    public void ChangeOpenableState(bool open) {
        if (!isLocked) {
            isOpen = open;
            StopAllCoroutines();
            if (changeAngle || isDoorColliding) StartCoroutine(RotateOpenable());
            if (changePosition) StartCoroutine(MoveOpenable());

            if (isOpen && !canCloseAfterOpening) gameObject.tag = "Untagged";
        } else {
            isOpen = false;
        }

    }

    public void LockOpenable (bool doesLock) {
        if (doesLock) {
            isLocked = true;
            ChangeOpenableState();
        } else {
            isLocked = false;
        }
    }

    private IEnumerator RotateOpenable () {
        isRotationChangingState = true;
        if (openRelativeToPlayer) {
            Vector3 heading = PlayerControllerMain.instance.transform.position - transform.position;
            if (openOppositeToPlayer) {
                openAngle.y = (Vector3.Dot(heading, transform.right) > 0) ? -Mathf.Abs(openAngle.y) : Mathf.Abs(openAngle.y);
            } else {
                openAngle.y = (Vector3.Dot(heading, transform.right) > 0) ? Mathf.Abs(openAngle.y) : -Mathf.Abs(openAngle.y);
            }
        }
        targetRotation = (!isOpen) ? Quaternion.Euler(closeAngle) : Quaternion.Euler(openAngle); //If the Openable is open close the Openable, if the Openable is closed open the Openable.
        while (Quaternion.Angle(targetRotation, transform.localRotation) > 0.25f) { //If the Openable is not within 0.25 of targetRotation continue slerping on the next frame.
            transform.localRotation = Quaternion.Lerp(transform.localRotation, targetRotation, smoothSpeed * Time.deltaTime); //Slerp the Openable towards the targetRotation.
            yield return null;
        }
        isRotationChangingState = false;
    }

    private IEnumerator MoveOpenable() {
        isPositionChangingState = true;
        targetPosition = (!isOpen) ? closeLocalPosition : openLocalPosition; //If the Openable is open close the Openable, if the Openable is closed open the Openable.
        while (Vector3.Distance(targetPosition, transform.localPosition) > 0.00125f) { //If the Openable is not within 0.25 of targetRotation continue slerping on the next frame.
            transform.localPosition = Vector3.LerpUnclamped(transform.localPosition, targetPosition, smoothSpeed * Time.deltaTime); //Slerp the Openable towards the targetRotation.
            yield return null;
        }
        isPositionChangingState = false;
    }

    public bool GetChangingState () {
        if (isPositionChangingState || isRotationChangingState) return true;
        return false;
    }

    //void OnTriggerEnter(Collider collider) {
    //    if (collider.tag == "Player") {
    //        isDoorColliding = true;
    //        StartCoroutine(WaitToStop(0));
    //    }
    //}

    //void OnTriggerExit(Collider collider) {
    //    if (collider.tag == "Player") {
    //        isDoorColliding = false;
    //    }
    //}

    //void OnCollisionEnter (Collision collision) {
    //    isDoorColliding = true;
    //    StartCoroutine(WaitToStop(0.125f));
    //}

    //private IEnumerator WaitToStop(float aSeconds) {
    //    yield return new WaitForSeconds(aSeconds);
    //    StopAllCoroutines();
    //}

    //void OnCollisionExit(Collision collision) {
    //    isDoorColliding = false;
    //}
}
