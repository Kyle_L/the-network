﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UNETSpawnManager : MonoBehaviour {
    public static UNETSpawnManager instance;

    [Header("General")]
    public Transform[] spawns;

    [Header("Settings")]
    [Tooltip("A spawn must be set at that index")]
    public int character1Spawn;
    [Tooltip("A spawn must be set at that index")]
    public int character2Spawn;
    [Tooltip("A spawn must be set at that index")]
    public int character3Spawn;
    [Tooltip("A spawn must be set at that index")]
    public int character4Spawn;

    private void Awake () {
        #region Enforces Singleton Pattern.
        //Check if instance already exists
        if (instance == null) {
            //if not, set instance to this
            instance = this;
        }
        //If instance already exists and it's not this:
        else if (instance != this) {
            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a MyNetworkManager.
            Destroy(gameObject);
        }
        #endregion
    }

    public Transform GetSpawn (Player.Character spawn) {
        switch (spawn) {
            case Player.Character.One:
                return spawns[character1Spawn];
            case Player.Character.Two:
                return spawns[character2Spawn];
            case Player.Character.Three:
                return spawns[character3Spawn];
            case Player.Character.Four:
                return spawns[character4Spawn];
            default:
                return spawns[0];
        }
    }
}
