﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Steamworks;

public class MyNetworkManager : NetworkManager {
    public static new MyNetworkManager singleton;

    public GameObject playerLobbyPrefab;

    private void Awake () {
        #region Enforces Singleton Pattern.
        //Check if instance already exists
        if (singleton == null) {
            //if not, set instance to this
            singleton = this;
        }
        //If instance already exists and it's not this:
        else if (singleton != this) {
            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a MyNetworkManager.
            Destroy(gameObject);
        }
        #endregion

        DontDestroyOnLoad(gameObject);
    }

    public override void OnClientConnect(NetworkConnection conn) {
        base.OnClientConnect(conn);
        UNETChat.instance.SetUp();
        //ClientScene.AddPlayer(conn, 0);
    }

    public override void OnServerDisconnect(NetworkConnection conn) {
        #region Object Despawning.
        //Creates an array of the instance ids of all the objects the player has authority of.
        NetworkInstanceId[] clientObjects = new NetworkInstanceId[conn.clientOwnedObjects.Count];
        conn.clientOwnedObjects.CopyTo(clientObjects);

        //Iterates through the array.
        foreach (NetworkInstanceId objId in clientObjects) {
            NetworkIdentity netIdentity = NetworkServer.FindLocalObject(objId).GetComponent<NetworkIdentity>();

            /*
            Removes authority from all NetworkIdenties and removes authority from them
            to ensure that objects the player has authority over will remain in the server
            after they disconnect. Please note this excludes their player object.
            */ 
            if (netIdentity.clientAuthorityOwner == conn) {
                netIdentity.RemoveClientAuthority(conn);
            }
        }

        /*
        Does the rest of the fancy disconnect stuff I don't know about
        including removing the player object.
        */
        base.OnServerDisconnect(conn);
        #endregion

        //UNETChat.instance.LeaveAnoucement(UNETPlayerManager.instance.GetPlayerName(conn.connectionId.ToString()));

        //UNETPlayerManager.instance.RemovePlayer(conn.connectionId.ToString());
        LobbyPlayerManager.instance.Remove(conn.connectionId.ToString());
    }

    public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId) {
        //Please note. need to access the player's directly as this following line is only on the server.
        if (LobbyPlayerManager.instance.IndexOf(conn.connectionId.ToString()) > 0 && !LobbyPlayerManager.instance.GetLobbyPlayer(conn.connectionId.ToString()).GetReady()) return;
        if (LobbyPlayerManager.instance.IndexOf(conn.connectionId.ToString()) < 0) {
            GameObject playerLobby = Instantiate(playerLobbyPrefab);
            NetworkServer.AddPlayerForConnection(conn, playerLobby, playerControllerId);
        } else {
            GameObject player = Instantiate(playerPrefab, UNETSpawnManager.instance.GetSpawn((Player.Character)LobbyPlayerManager.instance.GetLobbyPlayer(conn.connectionId.ToString()).myCharacterEnumIndex).position, UNETSpawnManager.instance.GetSpawn((Player.Character)LobbyPlayerManager.instance.GetLobbyPlayer(conn.connectionId.ToString()).myCharacterEnumIndex).rotation);
            if (!NetworkServer.AddPlayerForConnection(conn, player, playerControllerId))
                NetworkServer.ReplacePlayerForConnection(conn, player, 0);
        }
    }

    public void EnterGame (MyLobbyPlayer lobbyPlayer) {
        GameObject player = Instantiate(playerPrefab, UNETSpawnManager.instance.GetSpawn((Player.Character)lobbyPlayer.myCharacterEnumIndex).position, UNETSpawnManager.instance.GetSpawn((Player.Character)lobbyPlayer.myCharacterEnumIndex).rotation);
        NetworkServer.ReplacePlayerForConnection(lobbyPlayer.connectionToClient, player, 0);
    }


    public string GetLocalIpAddress () {
        return Network.player.ipAddress;
    }

    public string GetExternalIpAddress() {
        return Network.player.externalIP;
    }

    public void Host () {
        NetworkServer.Reset();
        StartHost();
    }
    
    public void Connect (InputField input) {
        //May or may not need.
        NetworkServer.Reset();
        networkAddress = input.text;
        StartClient();
    }

    public void Leave () {
        StopHost();
    }

    //#region Default 
    //private string playerName;

    //public void SetDefaultName(string aName) {
    //    playerName = aName;
    //}

    //public string GetDefaultName() {
    //    return playerName;
    //}

    //private PlayerMessage.character playerCharacter;

    //public void SetDefaultCharacter(PlayerMessage.character aName) {
    //    playerCharacter = aName;
    //}

    //public PlayerMessage.character GetDefaultCharacter() {
    //    return playerCharacter;
    //}
    //#endregion
}
