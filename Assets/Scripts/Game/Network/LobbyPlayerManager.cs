﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class LobbyPlayerManager : MonoBehaviour {
    public static LobbyPlayerManager instance;

    //[HideInInspector]
    public List<MyLobbyPlayer> players = new List<MyLobbyPlayer>();

    public delegate void PlayerAddedDelegate(MyLobbyPlayer player);
    public delegate void PlayerReadyDelegate(MyLobbyPlayer player);
    public delegate void PlayerRemovedDelegate(MyLobbyPlayer player);

    public event PlayerAddedDelegate PlayerAdded;
    public event PlayerReadyDelegate PlayerReady;
    public event PlayerRemovedDelegate PlayerRemoved;

    public void OnPlayerAdded(MyLobbyPlayer player) {
        PlayerAddedDelegate handler = PlayerAdded;
        if (handler != null) {
            handler(player);
        }
    }

    public void OnPlayerReady(MyLobbyPlayer player) {
        PlayerReadyDelegate handler = PlayerReady;
        if (handler != null) {
            handler(player);
        }
    }

    public void OnPlayerRemoved(MyLobbyPlayer player) {
        PlayerRemovedDelegate handler = PlayerRemoved;
        if (handler != null) {
            handler(player);
        }
    }

    private void Awake() {
        DontDestroyOnLoad(this);

        #region Enforces Singleton Pattern.
        //Check if instance already exists
        if (instance == null) {
            //if not, set instance to this	
            instance = this;
        }
        //If instance already exists and it's not this:
        else if (instance != this) {
            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a MyNetworkManager.
            Destroy(gameObject);
        }
        #endregion
    }

    /// <summary>
    /// Sets up the LobbyPlayerManager to its default state.
    /// </summary>
    public void SetUp() {
        //Finds all pre-existing MyLobbyPlayer.
        players.Clear();
        MyLobbyPlayer[] found = FindObjectsOfType<MyLobbyPlayer>();
        for (int i = 0; i < found.Length; i++) {
            if (!players.Contains(found[i])) players.Add(found[i]);
        }

        //Sets the handlers to null.
        PlayerAdded = null;
        PlayerReady = null;
        PlayerRemoved = null;
    }

    /// <summary>
    /// Adds a player.
    /// </summary>
    /// <param name="player"></param>
    public void AddPlayer (MyLobbyPlayer player) {
        players.Add(player);
        OnPlayerAdded(player); //Find a way to prevent null reference exception.
    }

    /// <summary>
    /// Removes a player
    /// </summary>
    /// <param name="id">The id of the player. From message.conn.connectionId.ToString();</param>
    public void Remove(string id) {
        PlayerRemoved(players[IndexOf(id)]);
        players.RemoveAt(IndexOf(id));
    }

    /// <summary>
    /// Removes a player
    /// </summary>
    /// <param name="player">A reference to the player object.</param>
    public void Remove(MyLobbyPlayer player) {
        PlayerRemoved(player);
        players.Remove(player);
    }

    public void SetPlayerReady(MyLobbyPlayer player) {
        player.SetInGame();
        OnPlayerReady(player);
    }

    /// <summary>
    /// Returns the number of players / size of the player list.
    /// </summary>
    /// <returns></returns>
    public int GetSize() {
        return players.Count;
    }

    /// <summary>
    /// Gets the index of the player within the list.
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public int IndexOf(string id) {
        for (int i = 0; i < players.Count; i++) { 
            if (players[i].connectionToClient.connectionId.ToString() == id) return i;
        }
        return -1;
    }

    /// <summary>
    /// Returns an instance of MyLobbyPlayer
    /// </summary>
    /// <param name="id">The id of the player. From message.conn.connectionId.ToString();</param>
    /// <returns></returns>
    public MyLobbyPlayer GetLobbyPlayer(string id) {
        return players[IndexOf(id)];
    }

    /// <summary>
    /// Returns an instance of MyLobbyPlayer
    /// </summary>
    /// <param name="index">The index of the player within the list.</param>
    /// <returns></returns>
    public MyLobbyPlayer GetLobbyPlayer(int index) {
        if (index < 0 || index > players.Count) return null;
        else return players[index];
    }

    /// <summary>
    /// Returns an instance of MyLobbyPlayer
    /// </summary>
    /// <param name="character">The the character of the player.</param>
    /// <returns></returns>
    public MyLobbyPlayer GetLobbyPlayer(Player.Character character) {
        foreach (MyLobbyPlayer player in players) {
            if (character == (Player.Character)player.myCharacterEnumIndex) return player;
        }
        return null;
    }

}
