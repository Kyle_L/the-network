﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class UNETSceneManager : NetworkBehaviour {
    //FIX, IT ONLY WORKS WITH HOST!!!
    [SyncVar] private bool isLoadingScene;
    
    void Update () {
        if (!isLoadingScene) {
            if (Input.GetKeyDown(KeyCode.Alpha1)) {
                LoadScene("Scene_Test");
            } else if (Input.GetKeyDown(KeyCode.Alpha2)) {
                LoadScene("Scene_Test2");
            } else if (Input.GetKeyDown(KeyCode.Alpha3)) {
                LoadScene("Scene_Test3");
            }
        }
    }

	void LoadScene (string sceneName) {
        PlayerControllerMain.instance.playerNetworkController.RequestAuthority(gameObject);
        CmdLoadScene(sceneName);
        isLoadingScene = true;
    }

    void LoadScene(int sceneNum) {
        LoadScene(SceneManager.GetSceneByBuildIndex(sceneNum).name);
    }

    [Command]
    void CmdLoadScene(string sceneName) {
        RpcLoadScene();
        StartCoroutine(ServerChangeSceneWait(sceneName));
    }

    private IEnumerator ServerChangeSceneWait(string newSceneName) {
        UIFadeTransitionController.instance.FadeUIIn();
        while (UIFadeTransitionController.instance.isFading) {
            yield return null;
        }
        NetworkManager.singleton.ServerChangeScene(newSceneName);
    }

    [ClientRpc]
    void RpcLoadScene() {
        if (!hasAuthority)
            UIFadeTransitionController.instance.FadeUIIn();
    }

}
