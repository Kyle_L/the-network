﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class Trigger_Subtitle : MonoBehaviour {

    public UISubtitleController subtitle;
    [Multiline]
    public string text;

    private void OnTriggerEnter(Collider other) {
        subtitle.SetNewText(text, Color.black);
        gameObject.SetActive(false);
    }
}
