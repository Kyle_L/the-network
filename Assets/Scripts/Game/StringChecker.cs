﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text.RegularExpressions;

public class StringChecker : MonoBehaviour {

    private static Regex r = new Regex("<.*?=|>");
	
    /// <summary>
    /// Returns a string with all RichText tags removed.
    /// </summary>
    /// <param name="aString">The initial string.</param>
    /// <returns>The string without any RichText tags.</returns>
	public static string RemoveRichText (string aString) {
        return r.Replace(aString, string.Empty);
	}
}
