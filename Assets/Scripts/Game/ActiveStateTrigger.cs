﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveStateTrigger : MonoBehaviour {

    GameObject[] active;
    GameObject[] inactive;

    private void OnTriggerEnter(Collider other) {
        foreach (GameObject g in active) {
            g.SetActive(true);
        }
        foreach (GameObject g in inactive) {
            g.SetActive(false);
        }
        gameObject.SetActive(false);
    }
}
