﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour {
    public static CameraManager instance;

    private Camera currentCamera;

    private Camera[] allCameras;

    public Camera[] sceneCameras;
    public int defaultSceneCamera;


    private void Awake () {
        if (instance == null) {
            //if not, set instance to this
            instance = this;
        }
        //If instance already exists and it's not this:
        else if (instance != this) {
            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a UIMenuController.
            Destroy(gameObject);
            return;
        }

    }

	private void Start () {
        //Gets all scene cameras.
        allCameras = Resources.FindObjectsOfTypeAll<Camera>();

        SetCamera(sceneCameras[defaultSceneCamera]);
    }

    public void SetCamera (Camera aCamera) {
        currentCamera = aCamera;

        for (int i = 0; i < allCameras.Length; i++) {
            allCameras[i].gameObject.SetActive(false);
        }
        aCamera.gameObject.SetActive(true);
	}

    public void SetCameraFade (Camera aCamera, float speed = 10) {
        StartCoroutine(LerpPosition(currentCamera, aCamera, speed));
    }

    IEnumerator LerpPosition (Camera aCamera, Camera aCamera2, float speed) {
        while (Vector3.Distance(aCamera.transform.position, aCamera2.transform.position) > 0.01f && Quaternion.Angle(aCamera.transform.rotation, aCamera2.transform.rotation) > 0.01f) {
            aCamera.transform.position = Vector3.Lerp(aCamera.transform.position, aCamera2.transform.position, Time.deltaTime * speed);
            aCamera.transform.rotation = Quaternion.Lerp(aCamera.transform.rotation, aCamera2.transform.rotation, Time.deltaTime * speed);
            yield return null;
        }
        SetCamera(aCamera2);
    }
}
