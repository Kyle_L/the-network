﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CustomFunctions : MonoBehaviour {

    // Note that Color32 and Color implictly convert to each other. You may pass a Color object to this method without first casting it.
    public static string ColorToHex(Color32 color) {
        string hex = color.r.ToString("X2") + color.g.ToString("X2") + color.b.ToString("X2");
        return hex;
    }

    public static Color HexToColor(string hex) {
        byte r = byte.Parse(hex.Substring(1, 2), System.Globalization.NumberStyles.HexNumber);
        byte b = byte.Parse(hex.Substring(3, 2), System.Globalization.NumberStyles.HexNumber);
        byte g = byte.Parse(hex.Substring(5, 2), System.Globalization.NumberStyles.HexNumber);
        byte a = byte.Parse(hex.Substring(7, 2), System.Globalization.NumberStyles.HexNumber);
        return new Color(r, g, b, a);
    }

    public static bool IsDigitsOnly(string str) {
        foreach (char c in str) {
            if (c < '0' || c > '9')
                return false;
        }
        return true;
    }

    private static string[] curseWords = { "fuck", "shit", "ass", "hell" };

    public static string CensorLanguage (string str) {
        for (int iB = 0; iB < curseWords.Length; iB++) {
            if (str.Contains(curseWords[iB])) {
                str = str.Replace(curseWords[iB], new string('*', curseWords[iB].Length));
            }
        }
        return str;
    }

    public static float ClampAngle(float angle, float min, float max) {
        if (angle < 90 || angle > 270) {       // if angle in the critic region...
            if (angle > 180) angle -= 360;  // convert all angles to -180..+180
            if (max > 180) max -= 360;
            if (min > 180) min -= 360;
        }
        angle = Mathf.Clamp(angle, min, max);
        if (angle < 0) angle += 360;  // if angle negative, convert to 0..360
        return angle;
    }

    /// <summary>
    /// Finds all the GameObjects that are of type layer.
    /// </summary>
    /// <param name="layer">The layer being searched for</param>
    public static GameObject[] FindAllGameObjectsOfTypeWithLayer(int layer) {
        GameObject[] allGo = GameObject.FindObjectsOfType(typeof(GameObject)) as GameObject[];
        List<GameObject> list = new List<GameObject>();
        for (int i = 0; i < allGo.Length; i++) {
            if (allGo[i].layer == layer) {
                list.Add(allGo[i]);
            }
        }
        return list.ToArray();
    }

    /// <summary>
    /// Sets all layers within a gameobject to a specific layer.
    /// </summary>
    /// <param name="gameObject">The gameobject that its layers are being changed.</param>
    /// <param name="layerNumber">The layer all gameobjects will be changed to.</param>
    public static void SetLayerRecursively(GameObject gameObject, int layerNumber) {
        if (gameObject == null) return;
        foreach (Transform child in gameObject.GetComponentsInChildren<Transform>(true)) {
            child.gameObject.layer = layerNumber;
        }
    }

    /// <summary>
    /// Checks if renderer is visible by camera.
    /// </summary>
    /// <param name="renderer">The renderer being checked if visible by the camera.</param>
    /// <param name="camera">The camera checking if the renderer is visible. </param>
    /// <returns></returns>
    public static bool IsVisibleFrom(Renderer renderer, Camera camera) {
        Plane[] planes = GeometryUtility.CalculateFrustumPlanes(camera);
        return GeometryUtility.TestPlanesAABB(planes, renderer.bounds);
    }

    public static bool IsVisibleFrom(GameObject go, Camera camera) {
        Vector3 temp = camera.WorldToViewportPoint(go.transform.position);
        return !(temp.x > 1 || temp.y > 1 || temp.z > 1);
    }

    /// <summary>
    /// Scale one object to fit into another object.
    /// </summary>
    /// <param name="scalingObject">The object being scaled.</param>
    /// <param name="otherObject">The object that the scaling object is scaling to.</param>
    public static void ScaleToFit (GameObject scalingObject, GameObject otherObject) {
        Collider[] scalingColliders = scalingObject.GetComponentsInChildren<Collider>();
        Bounds scalingBounds = scalingColliders[0].bounds;
        foreach (Collider c in scalingColliders) {
            scalingBounds.Encapsulate(c.bounds);
        }

        Collider otherColliders = otherObject.GetComponent<Collider>();
        Bounds otherBounds = otherColliders.bounds;
        //foreach (Collider c in otherColliders) {
        //    otherBounds.Encapsulate(c.bounds);
        //}
        
        Vector3 sizeA = otherBounds.size;
        Vector3 sizeB = scalingBounds.size;
        float xAxis = sizeA.x / sizeB.x;
        float yAxis = sizeA.y / sizeB.y;
        float zAxis = sizeA.z / sizeB.z;
        float scaleSize = (xAxis > yAxis) ? yAxis : xAxis;
        scaleSize = (scaleSize > zAxis) ? zAxis : scaleSize;
        Vector3 scale = new Vector3(scaleSize, scaleSize, scaleSize);
        scalingObject.transform.localScale = scale;
    }


    /// <summary>
    /// Compares the distance of two points relative to the player.
    /// </summary>
    /// <param name="a">The first GameObject being compared.</param>
    /// <param name="b">The second GameObject being compared.</param>
    /// <returns></returns>
    public static int CompareDistanceToPlayer(GameObject a, GameObject b) {
        return CompareDistanceToPlayer(a.transform.position, b.transform.position);
    }

    /// <summary>
    /// Compares the distance of two points relative to the player.
    /// </summary>
    /// <param name="a">The first Transform being compared.</param>
    /// <param name="b">The second Transform being compared.</param>
    /// <returns></returns>
    public static int CompareDistanceToPlayer(Transform a, Transform b) {
        return CompareDistanceToPlayer(a.position, b.position);
    }

    /// <summary>
    /// Compares the distance of two points relative to the player.
    /// </summary>
    /// <param name="a">The first point being compared.</param>
    /// <param name="b">The second point being compared.</param>
    /// <returns></returns>
    public static int CompareDistanceToPlayer (Vector3 a, Vector3 b) {
        float dstToA = Vector3.Distance(PlayerControllerMain.instance.transform.position, a);
        float dstToB = Vector3.Distance(PlayerControllerMain.instance.transform.position, b);
        return dstToA.CompareTo(dstToB);
    }

    public static int CompareDistanceToPlayer2(GameObject a, GameObject b) {
        return CompareDistanceToPlayer2 (a.transform.position, b.transform.position);

    }

    public static int CompareDistanceToPlayer2(Vector3 a, Vector3 b) {
        float dstToA = Vector3.Distance(PlayerControllerMain.instance.transform.position, a);
        float dstToB = Vector3.Distance(PlayerControllerMain.instance.transform.position, b);

        Vector3 headingA =  a - PlayerControllerMain.instance.transform.position;
        float dotA = Vector3.Dot(headingA, PlayerControllerMain.instance.transform.forward);
        Vector3 headingB = b - PlayerControllerMain.instance.transform.position;
        float dotB = Vector3.Dot(headingB, PlayerControllerMain.instance.transform.forward);

        if (dotA > 0 && dotB < 0) return -1;
        if (dotA < 0 && dotB > 0) return 1;
        return dstToA.CompareTo(dstToB);

    }

    /// <summary>
    /// Gets the index of the Nth occurence of a char in a string.
    /// </summary>
    /// <param name="aString">The string thats checked for the chars.</param>
    /// <param name="aChar">The char that's occurence is being checked.</param>
    /// <param name="n">The occurence that is being checked for.</param>
    /// <returns></returns>
    public static int GetNthIndex(string aString, char aChar, int n) {
        int count = 0;
        for (int i = 0; i < aString.Length; i++) {
            if (aString[i] == aChar) {
                count++;
                if (count == n) {
                    return i;
                }
            }
        }
        return -1;
    }

    public static float GetStringDisplayTime (string aString, int wordPerMinute) {

        float diplayTime = 0;

        const float AVERAGE_WORDS_PER_MINUTE = 115;
        const float WORDS_PER_MINUTE_MULTIPLIER_CHAR_TIME = 0.0325f;
        const float PERIOD_MULTIPLIER = 10f;
        const float COMMA_MULTIPLIER = 5f;

        float charTime = AVERAGE_WORDS_PER_MINUTE / wordPerMinute * WORDS_PER_MINUTE_MULTIPLIER_CHAR_TIME;

        char[] array = aString.ToCharArray();
        for (int i = 0; i < (aString.Length); i++) {
            float waitTime = 0;
            if (array[i] == '.') {
                waitTime = charTime * PERIOD_MULTIPLIER;
            } else if (array[i] == ',') {
                waitTime = charTime * COMMA_MULTIPLIER;
            } else {
                waitTime = charTime;
            }
            diplayTime += waitTime;
        }
        return diplayTime;
    }

}
