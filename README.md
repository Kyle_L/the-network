### What is this repository for? ###

* This repository is for the management and organization of The Network game.
* Version: Currently in prototype.

### How do I get set up? ###

* Ensure that you familiar with how to use BitBucket and Git.
* Checkout the branch dev-[your name]. If you don't see a branch
labeled with your name, please contact the Repo owner or admin.
* Follow the Contribution guidelines.

### Contribution guidelines ###

* Ensure that you use and only use the branches of the repository
that you are supposed to be using (Don't use someone else's branch). 
* Thoroughly comment/document your commits. 
* Document any issues in the issue tracker.
* Communicate in a professional and mature manner.

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact